/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution');

const path = require('node:path');
const createAliasSetting = require('@vue/eslint-config-airbnb/createAliasSetting');

module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-essential',
    //'@vue/eslint-config-airbnb',
    'eslint:recommended',
    '@vue/eslint-config-prettier/skip-formatting',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  rules: {
    'func-names': ['error', 'never'],
    'no-unneeded-ternary': [0],
    'no-unused-vars': 'off',
    //'no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
  },
  /* settings: {
    'import/resolver': {
      alias: {
        map: [['@', './src']],
      },
    },
  }, */
  settings: {
    ...createAliasSetting({
      '@': `${path.resolve(__dirname, './src')}`,
    }),
  },
};
