# Vue 3 Ecommerce Test

En este proyecto podrás ver un listado de productos, al hacer clic en alguno de ellos te mostrará una sección con información detallada. Pódrás iniciar sesión con un usuario y contraseña para luego poder comprar un producto.

## Especificaciones

Este proyecto esta hecho en vue 3 + vite y usa axios, vuex, vue-router, sass, bootstrap, cookie-js, eslint, prettier y vitest. Necesitas tener instalado nodejs (version >= 18) y npm.

## Instalación

1. Clona este repositorio en tu local
2. Ingresa a la carpeta del proyecto e instala los paquetes con npm, ejectuando el siguiente comando en la terminal.

```
npm install
```

3. Duplica el archivo .env.example que está en la raiz y renombralo por .env

4. Ejecuta los test unitarios

```
npm run test:unit
```

5. Compila el proyecto en modo desarrollo ejectuando el siguiente comando en la terminal.

```
npm run dev
```

6. Entra a la dirección que aparece en la terminal
7. Listo, puedes probarla.
