import { describe, it, expect } from 'vitest';
import { mount } from '@vue/test-utils';

import ProductCard from '../products/ProductCard.vue';
import router from '../../router';

describe('ProductCard', () => {
  it('render props', () => {
    const wrapper = mount(ProductCard, {
      props: {
        product: {
          thumbnail: 'thumb.jpg',
          title: 'product 1',
          description: 'Lorem Ipsum is simply dummy text of the printing and',
          id: 1,
        },
      },
      /* global: {
        plugins: [router],
      }, */
      global: {
        stubs: ['RouterLink', 'router-view'], // Stubs for router-link and router-view in case they're rendered in your template
      },
    });

    console.log(wrapper.html());
    expect(wrapper.html()).toContain('thumb.jpg');
    expect(wrapper.html()).toContain('product 1');
    expect(wrapper.html()).toContain('Lorem Ipsum is simply dummy text of the printing a ...');
    expect(wrapper.html()).toContain('/products/1');
  });
});
