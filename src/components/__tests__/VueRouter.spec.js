import { describe, it, expect, vi, beforeEach } from 'vitest';
import { mount } from '@vue/test-utils';
import { useRouter, useRoute } from 'vue-router';

import ProductCard from '../products/ProductCard.vue';
import router from '../../router';

const Component = {
  template: `<button @click="redirect">Click to Edit</button>`,
  props: ['isAuthenticated'],
  setup(props) {
    const router = useRouter();
    const route = useRoute();

    const redirect = () => {
      if (props.isAuthenticated) {
        //router.push(`/posts/${route.params.id}/edit`);
        router.push({
          name: 'greeting',
          query: {
            name: 'John Doe',
          },
        });
      } else {
        router.push('/404');
      }
    };

    return {
      redirect,
    };
  },
};

const ComponentTwo = {
  template: `<button @click="redirect">Click to Edit</button>`,
  props: ['isAuthenticated'],
  setup(props) {
    const router = useRouter();
    const route = useRoute();

    const redirect = () => {
      if (props.isAuthenticated) {
        router.push(`/posts/${route.params.id}/edit`);
        /* router.push({
          name: 'greeting',
          query: {
            name: 'John Doe',
          },
        }); */
      } else {
        router.push('/404');
      }
    };

    return {
      redirect,
    };
  },
};

vi.mock('vue-router');
describe('Vue router', () => {
  useRouter.mockReturnValue({
    push: vi.fn(),
  });

  beforeEach(() => {
    useRouter().push.mockReset();
  });

  it('allows authenticated user to edit a post Form 1', async () => {
    const wrapper = mount(Component, {
      props: {
        isAuthenticated: true,
      },
      global: {
        stubs: ['router-link', 'router-view'], // Stubs for router-link and router-view in case they're rendered in your template
      },
    });

    await wrapper.find('button').trigger('click');

    expect(useRouter().push).toHaveBeenCalledWith({
      name: 'greeting',
      query: {
        name: 'John Doe',
      },
    });
  });

  it('allows authenticated user to edit a post Form 2', async () => {
    useRoute.mockImplementationOnce(() => ({
      params: {
        id: 1,
      },
    }));

    const push = vi.fn();
    useRouter.mockImplementationOnce(() => ({
      push,
    }));

    const wrapper = mount(ComponentTwo, {
      props: {
        isAuthenticated: true,
      },
      global: {
        stubs: ['router-link', 'router-view'], // Stubs for router-link and router-view in case they're rendered in your template
      },
    });

    await wrapper.find('button').trigger('click');

    expect(push).toHaveBeenCalledTimes(1);
    expect(push).toHaveBeenCalledWith('/posts/1/edit');
  });
});
