import { useStore } from 'vuex';

export default (to, from) => {
  const store = useStore();
  // console.log('guest middleware');

  if (store.getters['auth/isAuthenticated']) {
    //next({ name: 'login' }); // next is still supported in case we want to use it
    return { name: 'login' };
  } else {
    // next(); // next is still supported in case we want to use it
    return true;
  }
};
