import { useStore } from 'vuex';

export default (to, from) => {
  const store = useStore();
  // console.log('user middleware');

  if (store.getters['auth/isAuthenticated']) {
    // next(); // next is still supported in case we want to use it
    return true;
  } else {
    // next({ name: "login" }); // next is still supported in case we want to use it
    return { name: 'login' };
  }
};
