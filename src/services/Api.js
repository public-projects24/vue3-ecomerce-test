import axios from 'axios';
// import router from "@/router";
// import store from "@/store";

const Api = axios.create({
  baseURL: import.meta.env.VITE_APP_SITE_API,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

async function interceptorOnError(error) {
  console.log('error interceptor ', error.response);
  // console.log("the route is: ", router.currentRoute);

  // If user is unauthenticated
  if (error.response.status === 401) {
    /* if (router.currentRoute.name != "login") {
              console.log("the route is different from login");
              await store.dispatch("auth/logout");
              router.push({ name: "login" });
          } */
  }
}

Api.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    interceptorOnError(error);
    return Promise.reject(error);
  }
);

export default Api;
