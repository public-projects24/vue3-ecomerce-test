import Api from '@/services/Api';

export default {
  getLimited(limit) {
    return Api.get(`/products?limit=${limit}`);
  },
  find(id) {
    return Api.get(`/products/${id}`);
  },
};
