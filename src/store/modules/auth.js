import Cookie from 'js-cookie';
import Auth from '@/services/Auth';

export default {
  namespaced: true,
  state: () => ({
    token: Cookie.get('token') ? Cookie.get('token') : null,
    user: Cookie.get('user') ? Cookie.get('user') : null,
  }),
  getters: {
    isAuthenticated(state) {
      return state.token != null ? true : false;
    },
    getToken(state) {
      return state.token;
    },
    getUser(state) {
      return state.user;
    },
    getAuthorizationHeaders(state) {
      return {
        headers: {
          Authorization: `Bearer ${state.token}`,
        },
      };
    },
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    setUser(state, user) {
      state.user = user;
    },
  },
  actions: {
    async login(context, form) {
      const { data } = await Auth.login(form);
      context.commit('setToken', data.token);
      context.commit('setUser', data.username);

      Cookie.set('token', data.token, { expires: 1 });
      Cookie.set('user', data.username, { expires: 1 });
    },
    async logout(context) {
      context.commit('setToken', null);
      Cookie.remove('token');
      context.commit('setUser', null);
      Cookie.remove('user');
    },
  },
};
