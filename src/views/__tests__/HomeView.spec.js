import { describe, it, expect, vi } from 'vitest';
import { mount, flushPromises, shallowMount } from '@vue/test-utils';
import Product from '../../services/Product';
import HomeView from '../HomeView.vue';

const mockProducts = {
  data: {
    products: [
      {
        description: 'An apple mobile which is nothing like apple',
        thumbnail: 'https://i.dummyjson.com/data/products/1/thumbnail.jpg',
        title: 'iPhone 9',
        id: 1,
      },
      {
        description:
          'SIM-Free, Model A19211 6.5-inch Super Retina HD display with OLED technology A12 Bionic chip with ...',
        thumbnail: 'https://i.dummyjson.com/data/products/2/thumbnail.jpg',
        title: 'iPhone X',
        id: 2,
      },
    ],
  },
};

// Following lines tell vi to mock any call to `Products.getLimited`
// and to return `mockProducts` instead
const spy = vi.spyOn(Product, 'getLimited').mockResolvedValue(mockProducts);

describe('HomeView', () => {
  it('loads products', async () => {
    const wrapper = mount(HomeView, {
      global: {
        stubs: ['RouterLink'], // Stubs for router-link in case they're rendered in your template
      },
    });

    expect(spy).toHaveBeenCalled();

    // Wait until the DOM updates.
    await flushPromises();

    // console.log(wrapper.html());
    expect(spy).toHaveReturnedWith(mockProducts);
  });
});
