import { describe, it, expect, vi, beforeEach } from 'vitest';
import { mount, flushPromises, shallowMount } from '@vue/test-utils';
import { useRouter, useRoute } from 'vue-router';
import LoginView from '../LoginView.vue';

vi.mock('vue-router');

describe('LoginView', () => {
  useRouter.mockReturnValue({
    push: vi.fn(),
  });

  beforeEach(() => {
    useRouter().push.mockReset();
  });

  const store = {
    /*     state: {
      count: 25,
    }, */
    dispatch: vi.fn(),
  };

  it('show error on empty fields', async () => {
    const wrapper = mount(LoginView, {
      global: {
        provide: {
          store,
        },
      },
    });

    await wrapper.find('form').trigger('submit');
    // console.log(wrapper.html());
    expect(wrapper.html()).contains('All fields are required');
  });

  it('redirect to home on successful login', async () => {
    const wrapper = mount(LoginView, {
      global: {
        provide: {
          store,
        },
      },
    });

    wrapper.get('[data-test="username"]').setValue('kminchelle');
    wrapper.get('[data-test="password"]').setValue('0lelplR');
    await wrapper.find('form').trigger('submit');

    expect(store.dispatch).toHaveBeenCalled();
    expect(useRouter().push).toHaveBeenCalledWith({
      name: 'home',
    });
  });
});
